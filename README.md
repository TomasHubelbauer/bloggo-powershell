# PowerShell

## Detached Process

```powershell
Start-Job { code . }
```

## Scheduled Tasks

Requires Administrator access.

- Help: Append `/?` after any command
- Create: `schtasks /create /sc onstart /tn Notepad /tr notepad`
- Change: `schtasks /change /sc onlogon /tn Notepad /tr notepad`
- List: `schtasks /query /fo list` (or `csv` or `table`), pipe to `find` if needed

Backing files are stored here: https://stackoverflow.com/a/20454183/2715716

- `onstart` is for services and scripts
- `onlogon` is for graphical applications

Schedules tasks do not appear to log anything to Event Viewer.

## Event Viewer

`eventvwr`

## Search

`dir /s *foo*` searches the current directory.

## Dell Service Tag

`wmic bios get serialnumber`
